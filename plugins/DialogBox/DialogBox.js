import DialogBox from '~/components/Info/DialogBox.vue'

const Plugin = {
    install(Vue, options = {}) {
        /**
         * Makes sure that plugin can be installed only once
         */
        if (this.installed) {
            return
        }
        this.installed = true

        /**
         * Create event bus
         */

        this.event = new Vue()
        this.promise = null

        /**
         * Plugin methods
         */
        Vue.prototype.$DialogBox = {
            async show(title, text) {
                const options = { title, text }
                return await new Promise((resolve) => {
                    Plugin.event.$emit('show', options)

                    Plugin.event.$on('result', (res) => {
                        resolve(res)
                    })
                })
            },
        }

        /**
         * Registration of <DialogBox/> component
         */
        Vue.component('DialogBox', DialogBox)
    },
}

export default Plugin
