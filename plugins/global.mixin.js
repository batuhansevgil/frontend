import Vue from 'vue'
import { mapGetters, mapActions } from 'vuex'

Vue.mixin({
  computed: {
    ...mapGetters({ config: 'view/config', channels: 'channel/channels', sensors: 'sensor/sensors' }),
  },
  methods: {
    ...mapActions({ sendSettingsToQueue: 'channel/sendSettingsToQueue' }),
  },
})
