import TheToastr from '~/components/Info/TheToastr.vue'

const Plugin = {
    install(Vue, options = {}) {
        /**
         * Makes sure that plugin can be installed only once
         */
        if (this.installed) {
            return
        }
        this.installed = true

        /**
         * Create event bus
         */

        this.event = new Vue()

        /**
         * Plugin methods
         */
        Vue.prototype.$toastr = {
            /**
             *
             * @param {type,title,text} options
             */
            show(options) {
                Plugin.event.$emit('show', options)
            },
        }

        /**
         * Registration of <TheToastr/> component
         */
        Vue.component('TheToastr', TheToastr)
    },
}

export default Plugin
