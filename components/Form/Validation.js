export const Validation = {
    required(property) {
        return (v) => !!v || `${property} is required`
    },
    minLength(property, minLength) {
        return (v) => (v && v.length > minLength) || `${property} must be less then ${minLength}`
    },
}
