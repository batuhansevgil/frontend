export const state = () => ({
    sensorData: [],
})
export const mutations = {
    setData(state, payload) {
        state.sensorData.length = 0
        state.sensorData.push(...payload)
    },
}

export const actions = {
    async fillSensorData({ commit }) {
        const data = await this.$axios.$get('sensor')

        commit('setData', data)
    },
    async add({ dispatch }, sensor) {
        const result = await this.$axios.$post('sensor', sensor)

        if (result) {
            this._vm.$toastr.show({ type: 'success', text: 'The sensor successfully added' })
        }
        dispatch('fillSensorData')
    },
    async delete({ dispatch }, sensor) {
        const confirmText = `Are you sure delete this sensor ${sensor.sensorName}`
        const confirm = await this._vm.$DialogBox.show('Delete Confirm', confirmText)

        if (confirm) {
            await this.$axios.$delete(`sensor/${sensor._id}`)

            this._vm.$toastr.show({ type: 'success', title: 'Delete Process', text: 'the process is successfully' })
        } else {
            this._vm.$toastr.show({ type: 'info', title: 'Delete Process', text: 'the processs canceled' })
        }
        dispatch('fillSensorData')
    },
    async changeSensorChannel({ dispatch }, { channel, sensorId }) {
        await this.$axios.$patch(`sensor/${sensorId}`, { channel })
        await dispatch('fillSensorData')
    },

    async isExist({ _ }, name) {
        const result = await this.$axios.$post(`sensor/exist`, { sensorName: name })
        return Boolean(result)
    },
}

export const getters = {
    sensors: (state) => state.sensorData,
}
