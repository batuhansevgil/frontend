import { ChannelMode } from '~/Types/global.types'

export const state = () => ({
    channels: [],
})
export const mutations = {
    // ? Maybe come from Backend
    setChannel(state, payload) {
        state.channels = payload || []
    },
    changeChannelUse(state, payload) {
        // TODO check this func.
        state.channels[payload].used = !state.channels[payload].used
    },
}

export const actions = {
    // ? Maybe come from Backend
    async fillChannel({ commit }) {
        const data = await this.$axios.$get('channel')
        commit('setChannel', data)
    },
    async releaseChannel({ dispatch }, id) {
        await this.$axios.$patch(`channel/${id}`, { _id: id, activeSensor: null, mode: ChannelMode.Idle })
        dispatch('fillChannel')
    },
    async freezeChannel({ dispatch }, { channelId, sensorId }) {
        await this.$axios.$patch(`channel/${channelId}`, { _id: channelId, activeSensor: sensorId, mode: ChannelMode.Wait })
        dispatch('fillChannel')
    },
    async previewChannel({ state, dispatch }, { channelId }) {
        const previews = state.channels.filter((i) => i.mode === ChannelMode.Preview)
        if (previews) {
            previews.forEach(async (element) => {
                await this.$axios.$patch(`channel/${element._id}`, { mode: ChannelMode.Idle, activeSensor: null, _id: element._id })
            })
        }
        await this.$axios.$patch(`channel/${channelId}`, { mode: ChannelMode.Preview })
        dispatch('fillChannel')
    },
    async listenChannel({ dispatch }, { channelId }) {
        await this.$axios.$patch(`channel/${channelId}`, { _id: channelId, mode: ChannelMode.Listen })
        dispatch('fillChannel')
    },
    async waitChannel({ dispatch }, { channelId }) {
        await this.$axios.$patch(`channel/${channelId}`, { _id: channelId, mode: ChannelMode.Wait })
        dispatch('fillChannel')
    },
    async sendSettingsToQueue({ state }, { channel, sensor, isActive }) {
        // refactor { sensor , isActive, channel}
        const sendItem = { sensor, channel, isActive }
        // await this.$axios.$post('channel/settings', sendItem)
        await this.$axios.$post('channel/preview', sendItem)
    },

    async sendSettingForPreview({ state }, { samplingRate, voltageRange, isActive }) {
        // refactor { sensor , isActive, channel}
        const sendItem = { samplingRate, voltageRange, isActive }
        // await this.$axios.$post('channel/settings', sendItem)
        await this.$axios.$post('channel/preview', sendItem)
    },

    async changeChannelMode({ dispatch }, { channelId, mode }) {
        await this.$axios.$patch(`channel/${channelId}`, { mode })
        dispatch('fillChannel')
    },
}

export const getters = {
    channels: (state) => state.channels,
}
