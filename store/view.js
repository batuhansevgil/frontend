export const state = () => ({
    config: {},
})
export const mutations = {
    setConfig(state, payload) {
        console.log('payload :>> ', payload)
        state.config = payload || {}
    },
}

export const actions = {
    async fillConfig({ commit }) {
        const data = await this.$axios.$get('view')

        commit('setConfig', data)
    },
}

export const getters = {
    config: (state) => state.config,
}
