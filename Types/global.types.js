export const EventName = {
  setSignal: 'setSignal',
  getSignal: 'getSignal',
}
export const QueueName = {
  signalQueue: 'signalQueue',
  currentSettings: 'currentSettings',
}

export const ChannelMode = {
  Idle: 0,
  Wait: 1,
  Preview: 2,
  Listen: 3,
}
export const sensorType = {
  inputType: 'inputType',
  voltageRange: 'voltageRange',
  rangeUnit: 'rangeUnit',
  antiAliasingFilter: 'antiAliasingFilter',
  twoPointCalibrationUnit: 'twoPointCalibrationUnit',
  excitationUnit: 'excitationUnit',
  firstPointUnit: 'firstPointUnit',
  offset: 'offset',
  scaleFactor: 'scaleFactor',
  secondPointUnit: 'secondPointUnit',
  secondPointVoltage: 'secondPointVoltage',
  decimalPoint: 'decimalPoint',
  samplingRate: 'samplingRate',
  firstPointVoltage: 'firstPointVoltage',
  excitation: 'excitation',
  description: 'description',
  channel: 'channel',
  sensorName: 'sensorName',
}
export const SingleConfigType = {
  text: String,
  value: Number,
}
export const ConfigType = {
  excitationUnits: Array,
  twoPointCalibrationUnit: Array,
  excitation: Array,
  antiAliasingFilter: Array,
  rangeUnits: Array,
  voltageRanges: Array,
  inputType: Array,
}
